# Stored Procedures

PSM: Persistent Stored Modules

PSMs are an extension to SQL, provided by the implementation, that allows us to write procedures and store them in the database as part of the schema

Why use a PSM?

- Code for maintaining the database stays with the database
- Saves round trips with data. Everything is executed on the database server
- Lets us do things we couldn't in just SQL

Each SQL implementation provides its own implementation of the language.

## Procedure declaration

``` 
CREATE PROCEDURE name (parameters)
<local declarations>
<procedure body>;
```

There's also `CREATE FUNCTION`, fundamentally the same, except it must return a value

Parameters are mode-name-type triples

Mode is
- `IN`
- `OUT`
- `INOUT` 

`IN` is default

Mode may be omitted

Function parameters may only be `IN`

## PL/pgSQL

We'll focus on PL/pgSQL, Postgres's procedural language

Postgres is very extensible. PSMs can be written in C, Perl, Tcl, and Python, as well as PL/pgSQL

It's also possible to create extensions in other languages. Open source options in Java, Lua, R, shell scripts, and JavaScript

## Syntax

```postgresql
CREATE FUNCTION name(params) RETURNS type 
AS 'function body text'
LANGUAGE plpgsql;
```
The function body is stored as a string-literal. By default, we'd then have to escape any single quotes or backslashes in the function body

We can use "dollar quoting" instead

Code is structured as blocks.

``` 
[<<label>>]
[DECLARE 
    declarations
]
BEGIN
    statements;
END [label]
```

Declarations and statements are terminated with a semi-colon `;`

Labels are usually optional, ending label is not required

Comments: `--`, `/* */`

Blocks can be nested, masking the outer block's variables. We can still access the outer block variables using the block's label. 

``` 
<<outerblock>>
DECLARE
    i
BEGIN
    i := 50;
    <<innerblock>>
    DECLARE
        i
    BEGIN
        i = outerblock.i - 25;
    END innerblock
END outerblock;
```

### Declarations

All variables used in a block must be declared

Variables can have any SQL datatype 

Also `ROW` and `RECORD` types.

`name type := expression;`

Other options as well

We can use previously defined types

`myGradeTuple grades%ROWTYPE`
`myGrade grades.grade%TYPE`

### Parameters

Parameters are named with `$1`, `$2`, etc

We can optionally give names 

`CREATE PROCEDURE foo(bar INT, baz INT OUT) AS $$...`

### Expressions

All expressions are processed using the server's SQL executor

Prepared Statement (which we won't cover)

### Basic Statements

Assignment: `variable := expression` (can also use `variable = expression` in Postgres)

To execute a statement with no result (`INSERT`) by simply including the statement. (Also have `PERFORM` keyword)

Single row result: `SELECT attributes INTO target FROM...` or `INSERT ... RETURNING expressions INTO target`

We can optionally use the `STRICT` keyword. Throws an error if other than one row is returned.

### Control Structures

`RETURN expression` 

Other variants: RETURN NEXT expression, RETURN QUERY query, etc. builds a result set which is returned later.

`RETURN` by itself exits the function and returns the constructed set.

#### Conditionals

`IF ... THEN ... END IF` (also `ELSE`, `ELSIF`)

`CASE ... WHEN ... THEN ... END CASE`

Case statements are similar to IF statements but throw an error is nothing is matched

#### Loops

More or less what you'd expect from a programming language

``` 
<<label>>
LOOP 
    statements
END LOOP label
```

Can use `EXIT` or `CONTINUE`, `EXIT WHEN ...`

``` 
WHILE boolean-expression LOOP
    statements
END LOOP
```

``` 
FOR name IN expression .. expression [BY expression] LOOP
    statements --can use name here
END LOOP
```

More useful is the ability to loop through query results

``` 
FOR semester IN SELECT semester FROM course LOOP
    statements involving semester
END LOOP
```

#### Errors

By default any error thrown will abort both the function and the surrounding transaction

You can have an `EXCEPTION` block after the statements but before the `END` to catch errors

Error conditions are defined by both name and `SQLSTATE`, similar to an enumerated type.

You can call `RAISE` with a number of different arguments to raise an exception. 

`ASSERT condition, message` will RAISE an error with the defined message if `condition` is false

```postgresql
CREATE OR REPLACE FUNCTION failing_students(IN course VARCHAR, IN course_semester CHAR(3))
RETURNS SETOF VARCHAR 
AS $$
DECLARE 
    failing_student student%rowtype;
BEGIN
    FOR failing_student IN 
        SELECT student.* 
        FROM student, (
            SELECT student_email
            FROM grades
            WHERE course_name = course AND grades.semester = course_semester
            GROUP BY student_email 
            HAVING AVG(grade) < .6
        ) failing
        WHERE student.email = failing.student_email 
    LOOP 
        RETURN NEXT failing_student.email;
    END LOOP;
    RETURN;
end;
$$ LANGUAGE plpgsql;

SELECT * FROM grades;

SELECT failing_students('Database Systems', 'S20');

```

# Triggers

A database/schema element

Sometimes called "event-condition-action rules"

Differ from previously discussed constraints:
- Only awakened by certain events
- Once awakened, the trigger tests a condition 
- If the condition holds, some action is performed

Might be something like 
- aborting a transaction
- updating a log table 

Can be potentially anything

The `CREATE TRIGGER` statement provides a number of options in the event, condition, and action parts.

Main features:
1. The check of the condition, and the action may be executed on either the state of the database before or after the triggering event is executed
2. The condition and action can refer to both the old and new values of tuples that were updated in the triggering event
3. It is possible to define events that are limited to a specific attribute or set of attributes
4. The programmer has the option of specifying that the trigger executes either:
    - Once for each modified tuple (row level trigger)
    - Once for each modifying statement that awakens the trigger (statement level trigger)

## Triggers in Postgres

Unlike standard SQL, where the `CREATE TRIGGER` statement contains the code for the action to be taken, in Postgres we define a special type of function that will be attached to the trigger

Much is the same

``` 
CREATE TRIGGER name
AFTER UPDATE [OF attr] ON table
REFERENCING
    OLD ROW as oldTuple,
    NEW ROW as newTuple
FOR EACH ROW 
WHEN condition
EXECUTE PROCEDURE func(args);
```

```postgresql
CREATE FUNCTION verify_grade_floor() RETURNS TRIGGER 
AS $$
BEGIN
    IF OLD.grade > NEW.grade
        THEN RETURN NULL;
        ELSE RETURN NEW;
    END IF;
end;
$$ LANGUAGE plpgsql;

CREATE TRIGGER grade_floor_trigger
BEFORE UPDATE OF grade ON grades 
FOR EACH ROW
EXECUTE PROCEDURE verify_grade_floor();

SELECT * FROM grades;


```

```postgresql


CREATE TABLE grade_log (
    student_email varchar(255),
    old_grade     float,
    new_grade     float,
    updated       TIMESTAMP
);

CREATE OR REPLACE FUNCTION log_grade_change() RETURNS TRIGGER
AS $$
BEGIN
    INSERT INTO grade_log(student_email, old_grade, new_grade, updated)
    VALUES (NEW.student_email, OLD.grade, NEW.grade, now());

    RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER grade_change_trigger
AFTER UPDATE ON grades
FOR EACH ROW
    WHEN (old.grade < new.grade)
EXECUTE PROCEDURE log_grade_change();

SELECT * FROM grade_log;

UPDATE grades SET grade = .75 WHERE student_email='bob@example.com' AND assignment ='HW-2';


```