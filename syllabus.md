# CSCI-4380 Database Systems

Database systems is an advanced undergraduate-level class intended to give students a strong foundation of knowledge related to modern database management systems.

## Course Information

Database Systems CSCI-4380

Mondays and Thursdays from 4:00pm - 5:50pm

The course will meet in DCC - 318

Course Website: [https://gitlab.com/samuelbjohnson/spring-2020-csci-4380](https://gitlab.com/samuelbjohnson/spring-2020-csci-4380)

Piazza Site: [https://piazza.com/rpi/spring2020/csci4380/home](https://piazza.com/rpi/spring2020/csci4380/home)

## Prerequisites

This course will involve a good amount of programming. It requires a good working knowledge of data structures and algorithms, and proficiency in a higher-level programming language (e.g., C++ or Python: equivalent of CSCI-2300). Homework assignments that require programming not covered in class will be in Python. No prior knowledge of database management systems is assumed. 

## Instructors

### Professor

**Samuel B. Johnson**

johnss20 \[at\] rpi \[dot\] edu

Office: AE 109

Office hours:
- Thursdays 6:00pm-8:00pm 
- By appointment

### TA's

**Haoyu Wu**

wuh8 \[at\] rpi \[dot\] edu

Office hours:
- Tuesdays & Fridays 8:00am-10am
- Location: Amos Eaton 118

**Xueyan Nie**
niex \[at\] rpi \[dot\] edu

Office hours:
- Monday 10:00am-12:00pm at Amos Eaton 118
- Wednesday 2:00pm-4:00pm at Amos Eaton 118

**Yitian Liu**

liuy57 \[at\] rpi \[dot\] edu

Office hours:
- Wednesday 12:00pm-2:00pm at Amos Eaton 127
- Thursday 10:00am-12:00pm at Amos Eaton 118

### Undergraduate Mentors

**Herta Calvo-Faugier**  
  
Office Hours:  
- Monday 10:00am-12:00pm at Amos Eaton 118  
- Friday 8:00am-10:00am at Amos Eaton 118  

**Gu Xinyu**

Office hours:
- Wednesday 12:00pm-2:00pm at Amos Eaton 127
- Wednesday 2:00pm-4:00pm at Amos Eaton 118

**Imrie Andrew**

Office hours:
- Monday 10:00am-12:00pm at Amos Eaton 118
- Thursday 10:00am-12:00pm at Amos Eaton 118
	
**Osama Minhas**

Office hours:
- Tuesdays & Fridays 8:00am-10:00am at Amos Eaton 118

**Nguyen Terry**

Office hours:
- Wednesday 12:00pm-2:00pm at Amos Eaton 127
- Wednesday 2:00pm-4:00pm at Amos Eaton 118


## Course Goals and Objectives

By the conclusion of this course, students should have learned how to:

- Apply principles of normalization to design a data model that leads to the development of high performance data-intensive applications
- Write correct and efficient code that implements application logic for high throughput data operations
- Understand how a database management system integrates into a larger application architecture
- Have a solid understanding of a variety of database paradigms, and a good idea of when each is appropriate

## Assessment Measures and Grading Criteria

This is a programming-intensive course, so a majority of your grade will be determined by your work on a number of individual programming assignments, and a number of lab assignments, which may be done in groups. There will also be two mid-term exams, each covering approximately a third of the semester, and one comprehensive final exam. Your grade will be determined as follows:

- Labs (15%). There will be approximately eight lab assignments throughout the semester. Labs will be done individually or in groups of two or three. Time will be provided for labs during the regularly-scheduled lectures, though the will not be due until the following day. A few will be written, but most will involve some element of programming, though some may simply involve properly installing and configuring software needed for the course. The percentage of your grade for each lab will vary, but labs will combine to make up fifteen percent of your grade.
- Homework (40%). There will be approximately eight homework assignments given throughout the semester. Homework assignments are to be done individually. Some collaboration on the overals strategy or technique for approaching a problem is allowed, but the work you submit for homework should be your own. Several assignments will be written, while the others will involve some element of programming. The percentage of your grade for each will vary, but homework will make up forty percent of your final grade.
- Exams (45%). There will be three exams given during the semester: two midterms, each worth 10% and covering roughly a third of the course each, and a comprehensive final worth 25% of your grade.

### Mid-Semester Update for Online Classes

Changes are summarized [here](online-classes-updates.md). Grades will now be calculated as follows:

- Labs (15%). There will still (hopefully) be eight lab assignments, though only six will be graded. Labs will still be done individually or in groups of two or three. You will generally be given two days to complete labs. Late days will now be allowed on labs, though they'll generally be capped at two.
- Homework (35%). There will now be approximately seven homework assignments. I'll still cap late day usage at 3 per assignments.
- Exams (10%). The single midterm exam that was given before the break will be worth 10%.
- Take-home Exam (10%). The take-home exam will be given in place of the other exams. It will be mostly comprehensive, though it will focus more on the middle third of the course.
- Final Project (30%). There will be a single final project, to be done in groups of four or five that will be worth of your grade.

## Texts

The course will rely extensively on **Database Systems The Complete Book (2nd Edition)** by Hector Garcia-Molina, Jeffrey D. Ullman, and Jennifer Widom. ISBN: 0131873253, Prentice Hall.

Additional readings will be assigned from a number of other online sources. All will be either freely available, or available for free to the RPI community.

## Communication

There is a [Piazza Site](https://piazza.com/rpi/spring2020/csci4380/home) for the course. Students are encouraged to use this as the primary means of communication between each other and with the instructors, including any questions related to the course material or assignments.

If there's something you don't feel comfortable sharing in piazza, instructors are available by email, or you can always stop by office hours or set up an appointment.

Announcements (e.g., changes to scheduled reading or homework assignments) will be posted on the piazza site.

## Other Course Policies

You are expected to communicate to the instructor any issue regarding your performance in the class ahead of time. This includes absence from exams, late homeworks, inability to perform an assigned task, problems with your team members, the need for extra time on exams, etc. You should be prepared to provide sufficient proof of any circumstances on which you are making a special request as outlined in the Rensselaer Handbook of Student Rights and Responsibilities, should it be requested.

### Attendance

Attendance is not required, though it is highly encouraged. Students are accountable for all material taught throughout the semester, and being absent when the material was covered is not an acceptable excuse for not knowing it.

### Late Policy

Late assignments create an extra burden on your TA and delay the discussion of the solutions in class. Homework assignments must be submitted electronically by the deadline, as measured by our computers. Assignments that are a minute late are considered a day late! Each student will be given a total of five days (whole or partial) of grace for late homework for the whole semester. Late days may not be used on labs. These grace days should be used carefully. Once the late days have been exhausted, late assignments may not be accepted without a written excuse from the Student Experience office.

### Grade Appeal Policy

If you disagree with the grading on a homework, you should appeal to the TA first to maintain consistency in grading. If you are not satisfied with the outcome, then you should appeal to the professor. For exam grading, appeal directly to the professor. Appeals must be made within one week after the specific grade is returned.

### Taking Exams

All exams will be open book and open notes. You may not use any electronic tools during exams including cell phones, tablets and calculators, and you may not share your notes with anybody during exams.

If you will be absent from exams or have other circumstances requiring exceptions, you must get an official excuse from the Student Experience office (se@rpi.edu).

## Academic Integrity

Student-teacher relationships are built on trust. For example, students must trust that teachers have made appropriate decisions about the structure and content of the courses they teach, and teachers must trust that the assignments that students turn in are their own. Acts that violate this trust undermine the educational process. The Rensselaer Handbook of Student Rights and Responsibilities defines various forms of Academic Dishonesty and you should make yourself familiar with these. In this class, all assignments that are turned in for a grade must represent the student’s own work. In cases where help was received, or teamwork was allowed, a notation on the assignment should indicate your collaboration.

Cheating and Academic Dishonesty will not be tolerated. All your coursework should provide an honest effort in solving the assigned problem by yourself (and by your group partners for group assignments). You are allowed to work with other students in designing algorithms, in interpreting error messages, in discussing strategies for finding bugs, but NOT in writing code or writing down solutions. Even if you discuss the problems with other students (or other teams), you should write down your own solution or program when turning in an assignment.

You may not share, copy, or discuss in detail code or solutions while writing it or afterwards. You may not show your code or solutions to other students as a means of helping them. You may not leave online, printed copies or drafts of your solutions in publicly accessible areas, such as labs, workstations, dorms, etc. You may not post complete or partial answers to homeworks in any public forum, especially on Piazza before the due date to make sure all students had a chance to work on the problems on their own.

All cases of cheating will be punished and reported to the Dean of Students. You will receive a grade of zero if you cheat on a homework. A second instance of cheating on a homework or any cheating in the exam will result in an F grade in this course. If you have any question concerning this policy before submitting an assignment, please ask for clarification. 

## Changes

There may be changes to the policies, deadlines and list of topics described in the syllabus. You can expect me to give you reasonable notice of any changes. All changes will be announced in class.
