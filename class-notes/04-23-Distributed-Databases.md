# Parallelism in Databases

Databases can generally benefit from parallelism

A parallel machine is basically a collection of processors
- Assume each processor also has a local memory cache

For databases, there are also a large number of disks
- Sometimes one per processor
- Sometimes disks may be accessible by all processors

Parallel systems also have some way of passing data between processors 

3 Broad Categories of system architectures
- Shared Memory
- Shared Disk
- Shared Nothing

## Shared Memory

Each processor has access to the memory of all the others. 

Single physical address space across the entire machine

## Shared Disk

Every processor has its own memory, but they still have access to all the disks

Disk controllers manage competing requests 

Two basic forms:
- Network Attached Storage (NAS): store and transfer files 
- Storage Area Network (SAN): store and transfer blocks

## Shared Nothing

Processors have their own memory and disks

All communication is done via a network, processor to processor. 

Shared nothing is the most common architecture

Relatively inexpensive to build

Algorithms must account for the cost of transferring data
- Typically there's a large fixed-cost for the message, and a small per-byte variable cost
- Try to send large amounts of data at once

## Map/Reduce Parallelism Framework

High-level framework that allows database processes to be written simply

User (developer) writes two functions: map, and reduce

A master controller divides the input data into chunks and assigns processors to run the map function on each chunk.

Other processors (maybe the same ones) perform the reduce function on pieces of the output of the map function

### Storage Model

Assume a massively parallel machine (shared nothing)

Data is stored in files
- Files are typically large (e.g. all the tuples for a relation)

Files are divided into chunks, possibly complete cylinders of a disk, typically many megabytes

Chunks are replicated for resiliency 

### Map/Reduce Functions

Key-value pairs -> map() -> key-value pairs -> sort by key -> reduce() -> output lists per intermediate key

**Map** function takes one key-value pair as input, and it produces a list of key-value pairs as output
- Types of outputs don't have to match the types of inputs
- "Keys" that are output aren't keys in a database sense. Many instances of the same key value can exist
- The map function often doesn't `return` a value in the normal sense. There's often an `emit()` function that's called multiple times

**Reduce** function takes a single intermediate key value, and a list of values associated with that key
- Duplicates are not eliminated 
- Output a single value for that key

Reduce function is often associative and commutative 
- Can be started before the map process is finished 

Example: Word count

``` 
map(docId, document) {
    for word in document:
        emit(word, 1)
}

reduce(word, list<count>) {
    insert (word, sum(list<count>) into the index 
}
```

Example: Inverted index for words in documents 

``` 
map(docId, document) {
    for word in document:
        emit(word, docId)
}

reduce(word, list<docId>) {
    remove duplicates from the list
    insert (word, list<docId>) into index
}
```

Example (in MongoDB) Find the population of each state: 

``` 
{
	"_id" : "01034",
	"city" : "TOLLAND",
	"loc" : [
		-72.908793,
		42.070234
	],
	"pop" : 1652,
	"state" : "MA"
}

//map
function() {
    emit(this.state, this.pop);
}

//reduce
function(key, values) {
    return Array.sum(values);
}

db.zips.mapReduce(function() {
                      emit(this.state, this.pop);
                  }, function(key, values) {
                         return Array.sum(values);
                     }, { "out": "state_population"}
);

```

 Example (in MongoDB): Count the number of zipcodes in each state
 
```
//map
function() {
    emit(this.state, 1);
}

//reduce
function(key, values) {
    return Array.sum(values)
}

db.zips.mapReduce(function() {
                      emit(this.state, 1);
                  }, function(key, values) {
                         return Array.sum(values)
                     }, { "out": "state_zipcode_count" }
);

```









## Distributed Databases

Different than parallel systems, in that the cost of communication is higher. A parallel system usually has processors on the same rack in the same data center; distributed databases are more geographically spread out.

Advantages:
- Same opportunity to parallelize the work
- Geographic distribution creates resiliency

Disadvantages:
- Cost of communication dominates the cost of in-memory operations. Algorithms need to account for that. 

### Distribution of Data

One reason to distribute data is because the data itself is already distributed. 
- Banks might have branches, the customers of a given branch have their data stored locally.
- Chain of stores might keep inventory data locally
- Digital library might keep copies of their own catalog

That means the data may be partitioned

`Courses(name, semester, location, time)`

The relation doesn't exist physically. It's the union of the relations from each campus, each with the same schema.

Local relations are called *fragments*. The partitioning of the data into the physical fragments is called *horizontal decomposition*. 

We can also partition a relation *vertically* by decomposing the relation into multiple relations, each with a subset of attributes. 

### Distributed Transactions

A transaction may now involve data at several sites, so the model of a transaction needs to change. 

A transaction now consists multiple *transaction components*.

2 Issues that need to be addressed:
- How do we manage abort/commit? What if one transaction component wants to abort, but the rest are fine? (Two-phased commit)
- How do we assure serializability of transactions? (Lock tables)

### Data Replication

An big advantage of distributed databases is easy replication of data.

It's good for both resiliency and speed of queries. 

Problems that must be faced:
- How do we keep the copies identical? An update to replicated data becomes a transaction that updates all copies. 
- How do we decide where and how many copies to make?
- What happens when there's a communication failure in the network?

### Distributed Commits

Given the database for a sales system at a store. We want to query the number of toothbrushes at each store, and issue shipping instructions to balance the inventory.

1. T0 created
2. T0 sends messages to start Ti transaction components
3. Each Ti executes a query and reports to T0
4. T0 accepts the responses and issues update instructions
5. Stores receive instructions and update

### Two-Phased commit:

Basic principle behind distributed transactions: either all of the transaction components execute, or none of them do.

Each component guarantees its own local atomicity

- Assume each site logs local actions, but there's global log
- One site is the *coordinator* and decides whether or not the transaction can commit
- Two-phase commit involves sending messages between the coordinator and the sites. Messages are logged at the sites to aid recovery.

We can describe the phases based on the messages sent. 

#### Phase 1

The coordinator decides when to attempt the commit.

1. The coordinator places `<Prepare T>` in its own log
2. The coordinator sends `Prepare T` message to each site
3. Each site receiving the message decides whether to commit or abort. The site may delay, but eventually it must send a response.
4. If a site wants to commit, it must enter the *precommitted* state. Once it's in that state, it cannot abort without a message from the coordinator.
    - Perform all the steps needed to ensure that T will not have abort, even if there's a system failure. 
    - Place `<Prepare T>` in its log
    - Send `Ready T` message to the coordinator
    - The commit is not done at this time
5. If the site wants to abort, it logs `<Don't commit T>` and sends `Don't Commit T` to the coordinator

#### Phase 2

Begins when Ready or Don't Commit responses are received.

A suitable timeout period is used, after which the coordinator assumes `Don't Commit`.

1. If the coordinator receives Ready responses from all sites, it decides to commit.
    - It logs `<Commit T>`
    - It sends `Commit T` to all sites
2. If one or more site responds with `Don't Commit`
    - It logs `Abort T`
    - Sends `Abort T` to all sites
3. If a site receives `Commit T`, it logs and commits
4. If a site receives `Abort T`, it logs and aborts