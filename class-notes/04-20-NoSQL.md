# NoSQL

Relational Databases have been around a long time, have lots of advantages, especially Query Performance

As computing has changed, we've moved from monolithic applications towards very distributed applications. 

Rigidity of SQL can be a disadvantage

NoSQL is the umbrella term for a growing number of alternatives to the traditional relational database

## Types of NoSQL

Basic types of NoSQL
- Document databases: key-value pairs, whose values are a complex type
- Graph Stores (Neo4J, Giraph)
- Key-value stores: (simplest NoSQL) Redis, BerkelyDB 
- Object storage: Amazon S3, Google Cloud Storage, OpenStack Swift
- Wide-Column Stores: databases that are optimized for very large datasets
    - Columns are stored together, rather than the rows
    - Cassandra, HBase


## Benefits of NoSQL

- Often more scalable
- Can have a higher performance (for certain operations)
- Data model handles things the relational model won't
    - Large volumes of rapidly changing unstructured data
    - Agile development with quick release cycle and frequent schema changes
- Geographically distributed databases
- Dynamic Schema
    - Schema doesn't have to be defined before you start adding data
        - Good for agile development, because you can modify the schema as development proceeds
        - With large relational databases, schema changes can involve downtime or complex upgrade/update procedures

## Differences

With NoSQL, the validation is often done by the application

### Sharding

Relational databases scale vertically: a single server hosts all the data to allow for joins, transactions, etc. 

Sharding involves splitting the data over a number of servers by splitting the data "horizontally," by moving some of the rows to a different database instance

Considerable performance gains are possible

Disadvantages:
- Heavier reliance on the interconnection between servers
    - Increased latency when querying
    - Data is only sharded on one axis
        - Some queries become slow or impossible
- Hard to reason about or guarantee the consistency of the data

NoSQL DBMS often support "auto-sharding" taking some of the complexity out. 

Distributed approach also makes replication of the data easier than many relational databases

## Disadvantages of NoSQL

- Transaction support is minimal 
- Data consistency is often not guaranteed
- Data must often be validated by the application 