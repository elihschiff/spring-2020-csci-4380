import beans.Course;

import java.sql.*;

public class BasicExample {

    public static void main(String[] args) {
        print("Starting");

        String name = "Database Systems";
        String semester = "S20";

        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/example_db?user=example_user&password=example-password"
        )) {
            //Statement statement = connection.createStatement();
            String query = "SELECT * FROM course WHERE name=? AND semester=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, semester);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()) {
                Course c = new Course();
                c.setName(rs.getString(1));
                c.setSemester(rs.getString("semester"));
                c.setLocation(rs.getString("location"));
                c.setTime(rs.getString(3));
                c.setCapacity(rs.getInt("capacity"));

                print(c.toString());
            }

            String insertQuery = "INSERT INTO student(name, email, major) VALUES('George', 'g1@example.com', 'ARCH')";
            PreparedStatement preparedStatement1 = connection.prepareStatement(insertQuery);
            ResultSet rs1 = preparedStatement1.executeQuery();
//            print("Updated " + String.valueOf(rows) + " rows in the database");
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    public static void print(String s) {
        System.out.println(s);


    }
}
